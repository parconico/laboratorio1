Funcion es_bisiesto <-bisiesto (a)
	Definir es_bisiesto, b4, b100, b400 Como Logico;
	b4<-a mod 4 =0;
	b100<-a mod 100=0;
	b400<-a mod 400=0;
	es_bisiesto <- (b4 & !b100) | b400;
FinFuncion




Proceso dias_anio
	definir mes, anio Como Entero;
	Escribir "Ingresar mes:';
	leer mes;
	Segun mes Hacer
		1,3, 5, 7, 8, 10, 12:
			Escribir "31 dias";
		2:
			Escribir "Ingrese a�o:";
			Leer anio;
			si bisiesto(anio) = Verdadero Entonces
				Escribir "29 dias";
			SiNo
				Escribir "28 dias";
			FinSi
		4, 6, 9, 11:
			Escribir "30 dias";
		De Otro Modo:
			Escribir "Mes no valido";
	FinSegun
FinProceso
