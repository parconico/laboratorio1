Proceso programaPi
	Definir n, i, T Como real;
	n<-0;
	Escribir "Ingrese Terminos:";
	Leer T;
	Para i<-1 Hasta T Con Paso 1 Hacer
		Si i % 2 = 0 Entonces
			n<-n-(4/((i*2)-1));
		SiNo
			n<-n+(4/((i*2)-1));
		FinSi
	FinPara
	Escribir n;
FinProceso
