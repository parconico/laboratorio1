Proceso riesgoPersona
	Definir E, P, A, IMC Como Real;
	Escribir "Ingresar estatura, peso y edad:";
	Leer E, P, A;
	IMC <- P/E^2;
	Si A<45 Entonces
		Si IMC<22 Entonces
			Escribir 'El riesgo es bajo';
		SiNo
			Si IMC>=22 Entonces
				Escribir "El riesgo es medio";
			FinSi
		FinSi
	SiNo
		Si A>=45 Entonces
			Si IMC<22 Entonces
				Escribir "El riesgo es medio";
			SiNo
				Si IMC>=22 Entonces
					Escribir "El riesgo es alto";
				FinSi
			FinSi
		FinSi
	FinSi
FinProceso
